El perfil del usuario debe tener un fichero en la que se definan las variables del target en cuestión
```bash
$ cat ~/.config/target/target.var 
declare -A TARGET
TARGET["HOST"]='SERVER-05'
TARGET["IP"]='10.10.1.30'
```
#!/bin/bash

source colorsAndStyles.sh

function readFileTarget(){
  # If exist nos devolvera la variable de type TARGET
  if [ -f "$HOME/.config/target/target.var" ] ; then
    source $HOME/.config/target/target.var
  fi
  
  for key in "${!TARGET[@]}" ; do
    value="${TARGET[$key]}"
    echo -e "${GREEN}${BOLD}${key}${NORMAL}: ${value}"
  done
}

while true; do
  clear
  readFileTarget
  sleep 3

done